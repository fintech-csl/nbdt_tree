# LOAD TREE ---------------------------------------------------------------

load(file="pruned_GPRT.RData")
load(file="pruned_median_tree_central.RData")
load(file="pruned_median_tree.RData")
load(file="pruned_mean_tree_central.RData")
load(file="pruned_mean_tree.RData")

# FUNCTIONS ---------------------------------------------------------------

#Computation of the median, the mean and the proportion on each leaf
stats_on_leaf=function(tree,threshold){
  Leafs=sort(unique(tree$where))
  N_leafs=length(Leafs)
  res=matrix(nrow=5,ncol=N_leafs)
  rownames(res)=c("Median","Mean","Proportion","Extreme Proportion","Proportion of extreme")
  colnames(res)=as.character(1:N_leafs)
  for(i in 1:N_leafs){
    stats_on_leaf=c(median(tree$y[tree$where==Leafs[i]]),
                    mean(tree$y[tree$where==Leafs[i]]),
                    sum(tree$where==Leafs[i])/length(tree$y),
                    sum(tree$y[tree$where==Leafs[i]]>threshold)/sum(tree$y>threshold),
                    sum(tree$y[tree$where==Leafs[i]]>threshold)/length(tree$y[tree$where==Leafs[i]]))
    res[,i]=stats_on_leaf 
  }
  return(res)
}


#library(truncdist)
library(fitdistrplus)

#Definition of the density and the distribution function of a truncated log-normal law, under low and above upp
dtlnorm <- function(x, meanlog, sdlog, low, upp)
{
  PU <- plnorm(upp, meanlog=meanlog, sdlog=sdlog)
  PL <- plnorm(low, meanlog=meanlog, sdlog=sdlog)
  dlnorm(x, meanlog, sdlog) / (PU-PL) * (x >= low) * (x <= upp) 
}
ptlnorm <- function(q, meanlog, sdlog, low, upp)
{
  PU <- plnorm(upp, meanlog=meanlog, sdlog=sdlog)
  PL <- plnorm(low, meanlog=meanlog, sdlog=sdlog)
  (plnorm(q,  meanlog, sdlog)-PL) / (PU-PL) * (q >= low) * (q <= upp) + 1 * (q > upp)
}

#Fit of a truncated log-normal above u on each leaf of a tree
fit_on_leaf=function(tree,u,cst_norm){
  Leafs=sort(unique(tree$where))
  N_leafs=length(Leafs)
  
  res=matrix(nrow=N_leafs,ncol=2)
  rownames(res)=as.character(1:N_leafs)
  colnames(res)=c("mu","sigma")
  
  for(i in 1:N_leafs){
    
    data=tree$y[tree$where==Leafs[i] & tree$y <= u]
    data=data*cst_norm
    fit=fitdist(data, "tlnorm",start = list(meanlog=0, sdlog=1),fix.arg=list(low=0, upp=u),method = "mle",optim.method="Nelder-Mead")
    
    #est_adjusted=c(fit$estimate[1],fit$estimate[2]+log(div))
    est_adjusted=c(fit$estimate[1],fit$estimate[2])
    cov_adjusted=vcov(fit)
    lower_bound_est_adjusted=round(est_adjusted-qnorm(0.975)*sqrt(diag(cov_adjusted)),digits=2)
    higher_bound_est_adjusted=round(est_adjusted+qnorm(0.975)*sqrt(diag(cov_adjusted)),digits=2)
    length_confidence_interval=round(higher_bound_est_adjusted-lower_bound_est_adjusted,digits=2)
    
    res[i,1]=paste0(round(est_adjusted[1],digits = 2)," [",lower_bound_est_adjusted[1],";", higher_bound_est_adjusted[1],"] (" ,  length_confidence_interval[1] ,")")
    res[i,2]=paste0(round(est_adjusted[2],digits = 2)," [",lower_bound_est_adjusted[2],";", higher_bound_est_adjusted[2],"] (" ,  length_confidence_interval[2] ,")")
  }
  return(res)
}


#FIT_ON_MEDIAN_TREE_CENTRAL=fit_on_leaf(tree = Median_tree,u=27799)

#FIT_ON_MEDIAN_TREE=fit_on_leaf(tree = Median_tree,u = +Inf)

fit_on_leaf_extreme=function(tree){
  Leafs=sort(unique(tree$where))
  N_leafs=length(Leafs)
  
  res=matrix(nrow=N_leafs,ncol=2)
  rownames(res)=as.character(1:N_leafs)
  colnames(res)=c("sigma","gamma")
  
  for(i in 1:N_leafs){
    
    data=tree$y[tree$where==Leafs[i]]
    fit=evir::gpd(data, threshold = 0, method = "ml")
    
    est_adjusted=fit$par.ests
    cov_adjusted=fit$varcov
    
    lower_bound_est_adjusted=round(est_adjusted-qnorm(0.975)*sqrt(diag(cov_adjusted)),digits=2)
    higher_bound_est_adjusted=round(est_adjusted+qnorm(0.975)*sqrt(diag(cov_adjusted)),digits=2)
    length_confidence_interval=round(higher_bound_est_adjusted-lower_bound_est_adjusted,digits=2)
    
    res[i,1]=paste0(round(est_adjusted[1],digits = 2)," [",lower_bound_est_adjusted[1],";", higher_bound_est_adjusted[1],"] (" ,  length_confidence_interval[1] ,")")
    res[i,2]=paste0(round(est_adjusted[2],digits = 2)," [",lower_bound_est_adjusted[2],";", higher_bound_est_adjusted[2],"] (" ,  length_confidence_interval[2] ,")")
   }
  return(res)
}

#FIT_ON_GENERALIZED_PARETO_TREE=fit_on_leaf_extreme(tree = GPRT)

# EXEC --------------------------------------------------------------------

#Plot of the trees
png(file = "fig_3_trees.png", width = 1600, height = 500)
par(mfrow=c(1,2))
plot(Mean_tree,margin=0.023)
try(text(Mean_tree,minlength=3,cex=0.75),silent = TRUE)
#plot(Median_tree,margin=0.024)
#try(text(Median_tree,minlength=3,cex=0.75),silent = TRUE)
dev.off()

png(file = "fig_5_tree.png", width = 800, height = 500)
par(mfrow=c(1,1))
plot(Mean_tree,margin=0.0255)
plot(GPRT,margin=0.025)
try(text(GPRT,minlength=3,cex=0.75),silent = TRUE)
dev.off()

png(file = "fig_6_trees.png", width = 1600, height = 500)
par(mfrow=c(1,2))
plot(Mean_tree_central,margin=0.026)
try(text(Mean_tree_central,minlength=3,cex=0.75),silent = TRUE)
plot(Median_tree_central,margin=0.027)
try(text(Median_tree_central,minlength=3,cex=0.75),silent = TRUE)
dev.off()

#Computation of the median, mean, the proportion, the extreme proportion (that is the number of excesses in the leaf over the number of excesses) and the proportion of extreme (that is the number of excesses in the leaf over the number of data in the leaf)
stats_desc_mean_tree=stats_on_leaf(Mean_tree,threshold=27999)
write.csv2(stats_desc_mean_tree,"fig_3_stats_mean_tree.csv")
stats_desc_median_tree=stats_on_leaf(Median_tree,threshold=27999)
write.csv2(stats_desc_median_tree,"fig_3_stats_median_tree.csv")
stats_desc_mean_tree_central=stats_on_leaf(Mean_tree_central,threshold=27999)
write.csv2(stats_desc_mean_tree_central,"fig_6_stats_mean_tree.csv")
stats_desc_median_tree_central=stats_on_leaf(Median_tree_central,threshold=27999)
write.csv2(stats_desc_median_tree_central,"fig_6_stats_median_tree.csv")

#Computation of the variable importance
variable_importance_trees=matrix(nrow=5,ncol=4)
rownames(variable_importance_trees)=c("Mean_tree","Median_tree","GPRT","Mean_tree_central","Median_tree_central")
colnames(variable_importance_trees)=c("Source","Type of breach","Type of organization","Year")
variable_importance_trees[1,]=Mean_tree$variable.importance[c("Gather.Information.Source","Type.of.breach","Type.of.organization","year(Date.Made.Public)")]/sum(Mean_tree$variable.importance)
variable_importance_trees[2,]=Median_tree$variable.importance[c("Gather.Information.Source","Type.of.breach","Type.of.organization","year(Date.Made.Public)")]/sum(Median_tree$variable.importance)
variable_importance_trees[3,]=GPRT$variable.importance[c("Gather.Information.Source","Type.of.breach","Type.of.organization","year(Date.Made.Public)")]/sum(GPRT$variable.importance)
variable_importance_trees[4,]=Mean_tree_central$variable.importance[c("Gather.Information.Source","Type.of.breach","Type.of.organization","year(Date.Made.Public)")]/sum(Mean_tree_central$variable.importance)
variable_importance_trees[5,]=Median_tree_central$variable.importance[c("Gather.Information.Source","Type.of.breach","Type.of.organization","year(Date.Made.Public)")]/sum(Median_tree_central$variable.importance)
write.csv2(variable_importance_trees,"table_5_and_table_8_variable_importance.csv")

#Estimates of the (truncated) log-norrmal law fit on each leaf

LN_parameters_median=fit_on_leaf(Median_tree,u=+Inf,cst_norm = 1)
LN_parameters_mean=fit_on_leaf(Mean_tree,u=+Inf,cst_norm = 1)
write.csv2(rbind(LN_parameters_median,LN_parameters_mean),"table_6.csv")

LN_parameters_median_central=fit_on_leaf(Median_tree_central,u=27799,cst_norm = 1)
LN_parameters_mean_central=fit_on_leaf(Mean_tree_central,u=27799,cst_norm = 1)
write.csv2(rbind(LN_parameters_median_central,LN_parameters_mean_central),"table_7.csv")

#Estimates of the Generalized Pareto Distribution fit on each leaf
GPD_GPRT=fit_on_leaf_extreme(GPRT)
write.csv2(GPD_GPRT,"table_19.csv")