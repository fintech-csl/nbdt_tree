#SETWD
#Working directory setting
#setwd("")
options(stringsAsFactors = FALSE)

#PACKAGES LOADING/INSTALLING
library(doParallel)
library(dplyr)
library(evir)
library(fastDummies)
library(fitdistrplus)
library(foreach)
library(gamlss)
library(gamlss.tr)
library(ggplot2)
library(glmnet)
library(lubridate)
library(mgcv)
library(QRM)
library(rpart)
library(stringr)
library(testit)
library(tidyr)
library(treeClust)
#The package truncdensity is attached as .tar.gz and is needed for the simulation
library(truncdensity)
library(truncdist)

#Data base loading and preprocessing
source("./exec_PRC_loading.R")
print("exec_PRC_loading.R")
#Frequency
source("./exec_PRC_frequency.R")
print("exec_PRC_frequency.R")

#Severity
source("./exec_PRC_severity.R")
print("exec_PRC_severity.R")
source("./function_trees.R")
print("function_trees.R")
source("./exec_trees.R")
print("exec_trees.R")
source("./exec_stats_trees.R")

print("exec_stats_trees.R")

#Comparison of extreme value theory methodologies
source("./exec_vs_GAM_tail.R")
print("exec_vs_GAM_tail.R")
source("./exec_vs_GAM_central.R")
print("exec_vs_GAM_central.R")
source("./exec_vs_GAM_output.R")
print("exec_vs_GAM_output.R")

#Simulation
source("./pricing_application.R")
print("pricing_application.R")
source("./exec_pricing_application.R")

print("exec_pricing_application.R")

source("./actuarial_framework_application.R")
print("actuarial_framework_application.R")
source("./exec_actuarial_framework_application.R")
