
import numpy as np 


def formatted_data_simu(x, t, e, z=None, idx=None):
    
    death_time = np.array(t[idx], dtype=float)
    censoring = np.array(e[idx], dtype=float)
    latent_z = np.array(z[idx], dtype=float)
    covariates = np.array(x[idx], dtype=float)

    if len(death_time.shape)>=2:
        death_time=death_time.squeeze(0)

    if len(censoring.shape)>=2:
        censoring=censoring.squeeze(0)

    if len(latent_z.shape)>=2:
        latent_z=latent_z.squeeze(0)

    if len(covariates.shape)>=3:
        covariates=covariates.squeeze(0)
        
    #print("observed fold:{}".format(sum(e[idx]) / len(e[idx])))
    survival_data = {'x': covariates, 't': death_time, 'e': censoring, 'z':latent_z}

    
    return survival_data 
